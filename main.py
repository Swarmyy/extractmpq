from mpyq import MPQArchive
from collections import OrderedDict
import xmltodict


def recursive_dict(d):
    for k, v in d.items():
        if isinstance(v, dict):
            recursive_dict(v)
        elif isinstance(v, list):
            for i in v:
                recursive_dict(i)
        else:
            print(f"{k} : {v}")


def norm_types(root: dict):
    catalog = root['Catalog']
    for k, v in catalog.items():
        if isinstance(v, dict):
            catalog[k] = [v]


def slug_subcat(subcat: dict):
    text = "\t"
    for k, v in subcat.items():
        """if isinstance(v, dict):
            print_odict(v, k, '\t')
            continue"""
        text += f"{v}\t"
    return text[:-1]


def print_odict(od: dict, title: str, prefix=""):
    if len(od) > 1:
        print(f"{prefix}{title}:")
        print(f"{prefix}{slug_subcat(od)}")
    else:
        print(f"{prefix}{title}: {list(od.values())[0]}")


def print_unit(unit: dict):
    for k, v in unit.items():
        if isinstance(v, dict):
            print_odict(v, k)
            continue
        if isinstance(v, list):
            print(f"{k}:")
            for m in v:
                if isinstance(m, dict):
                    print(slug_subcat(m))
                    continue
                print(m)
            continue

        print(f"{k} : {v}")


def main():
    # HeptaCraftBySwarm.SC2Mod # problem: the screen file can't be decompressed
    # TODO: try catch ce probleme de décompression quand ca arrive et juste continuer à les read manuellement
    # UM_advanced.SC2Mod
    # HeptaCraftBySwarm.SC2Mod
    archive = MPQArchive(r'HeptaCraftBySwarm.SC2Mod')
    unit_dict = xmltodict.parse(archive.read_file(b"Base.SC2Data\GameData\UnitData.xml"))
    norm_types(unit_dict)

    abil = archive.read_file(b"Base.SC2Data\GameData\AbilData.xml")
    abil_dict = xmltodict.parse(abil)
    norm_types(abil_dict)

    gamedata = archive.read_file(b"Base.SC2Data\GameData\GameData.xml")
    gamedata_dict = xmltodict.parse(gamedata)
    norm_types(gamedata_dict)

    weapons = xmltodict.parse(archive.read_file(b"Base.SC2Data\GameData\WeaponData.xml"))
    norm_types(weapons)

    # print(unit_dict['Catalog'])
    # disp_dict = unit_dict['Catalog']
    disp_dict = abil_dict['Catalog']
    # disp_dict = weapons['Catalog']
    for type_name, type_objects in disp_dict.items():
        print(f"\n__{str(type_name).upper()}__\n")
        for member in type_objects:
            print_unit(member)
            print("\n")

    # print(archive.print_files())


if __name__ == '__main__':
    main()
